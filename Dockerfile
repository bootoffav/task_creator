FROM php
ADD --chmod=0755 https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN install-php-extensions imap
RUN apt update && apt install unzip zip -y

WORKDIR /root/task_creator
RUN mkdir attachments
COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY exec/web_request exec/web_request
COPY src src
COPY composer.* .

RUN composer install
ENTRYPOINT ["exec/web_request"]
