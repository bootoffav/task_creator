<?php
declare(strict_types=1);

use bootoffav\XMT\b24_task_creator\parsers\WebRequestEmailParser;
use PHPUnit\Framework\TestCase;

class WebRequestEmailParserTest extends TestCase
{

    protected function setUp()
    {
        $this->emails = [
            [
                'type' => 'html',
                'body' => 'Created at: 2017-05-28 15:18:26
                                            Name: SAJI MATHWE
                                            E-mail: arkangi@eim.ae
                                            Telephone: +971507872603
                                            Company name: Arkan Garments Industries FZE
                                            Country: United Arab Emirates (UAE)
                                            Request: We are looking for 100% Polyester Oxford fabrics with specifications of 130 GSM ,
                                            Form ID: Mail form
                                            Page: xmtextiles.com/en/products/oxford-135gsm-plain-150d-300d-polyester-fabric-detail.html
                                            IP: 217.165.230.81',
                'email' => 'arkangi@eim.ae',
                'companyName' => 'Arkan Garments Industries FZE',
                'country' => 'United Arab Emirates (UAE)'
            ],
            [
                'type' => 'plain',
                'body' => 'Begin forwarded message:

                From: angel momo <angelmomo1972@gmail.com>
                Subject: Interes comercial
                Date: 22 May 2017 at 09:19:55 GMT+3
                To: spain@xmtextiles.com

                Hola somos una empresa de Alicante que nos dedicamos a la confección de productos varios. Hemos visto su página y estamos interesados en recibir sus catalo gos.
                Sin otro particular, reciba un cordial saludo.
                Atentamente
                Angel',
                'email' => 'angelmomo1972@gmail.com',
                'companyName' => '',
                'country' => ''
            ],
            [
                'type' => 'plain',
                'body' => 'Begin forwarded message:
                    From: "Maab" <maab@luckytextilemills.biz>
                        Subject: WORKWEAR FABRIC
                    Date: 17 May 2017 at 10:59:23 GMT+3
                    To: <info@xmtextiles.com>
                        Cc: <shakil@luckytextilemills.biz>
                        Hi Concerned:',
                'email' => 'maab@luckytextilemills.biz',
                'companyName' => '',
                'country' => ''
            ],
            [
                'type' => 'plain',
                'body' => 'Begin forwarded message:
                    From: info@npwear.dk
                    Subject: ASTM F1506-10a and ESD fabric
                    Date: 23 March 2015 at 14:00:24 GMT+3
                    To: info@xmtextiles.com
                    To whom it may concern,
                    I am looking for below fabrics:',
                'email' => 'info@npwear.dk',
                'companyName' => '',
                'country' => ''
            ]
        ];
        $this->WebRequestEmailParser = (
            new class extends WebRequestEmailParser
            {
                public function _getFromAddress(string $message_body, $type) : string
                {
                    return static::getFromAddress($message_body, $type);
                }

                public function _getCompanyName(string $message_body) : string
                {
                    return static::getCompanyName($message_body);
                }

                public function _getCountry(string $message_body) : string
                {
                    return static::getCountry($message_body);
                }
            }
        );
    }

    /** @test */
    public function parses_email_address()
    {
        foreach ($this->emails as $email) {
            $this->assertEquals(
                $email['email'],
                $this->WebRequestEmailParser->_getFromAddress(
                    $email['body'],
                    $email['type']
                )
            );
        }
    }

    /** @test */
    public function parses_company_name()
    {
        foreach ($this->emails as $email) {
            $this->assertEquals(
                $email['companyName'],
                $email['type'] === 'html' ? $this->WebRequestEmailParser->_getCompanyName($email['body']) : ''
            );
        }
    }

    /** @test */
    public function parses_country()
    {
        foreach ($this->emails as $email) {
            $this->assertEquals(
                $email['country'],
                $email['type'] === 'html' ? $this->WebRequestEmailParser->_getCountry($email['body']) : ''
            );
        }
    }
}