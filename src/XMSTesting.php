<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

use bootoffav\XMT\b24_task_creator\Traits\B24DiskTalker;
use bootoffav\XMT\b24_task_creator\XMS_TaskDescriptionMaker;

class XMSTesting extends TaskManager
{
    use B24DiskTalker;
    // array of folder to run checking for tasks
    public $folders;
    public $dbc;
    protected $checking_period_days = 100;

    public function __construct($dbc, $folder_id = null)
    {
        $this->dbc = $dbc;

        $foldersWithinRoot = $this->onlyFolders(CFG['XMSTesting']['base_folder']);

        if (is_null($folder_id)) {
            $lastFolders = $this->getLastFolders($foldersWithinRoot);
            $this->folders = $this->filterRecentlyCreatedFolder($lastFolders);
        } else {
            $this->folders = array_filter($foldersWithinRoot, function($folder) use ($folder_id) {
                return $folder->ID == $folder_id ? true : false;
            });
        }
    }

    public function createTask(array $folder, array $fileList) : \stdClass
    {
        return $this->curlPostRequest(
            '&&0[TITLE]='.urlencode($this->makeTaskTitle($folder))
            .'&0[DESCRIPTION]='.urlencode(
                (new XMS_TaskDescriptionMaker())->makeTaskDescription($folder, $fileList)
            ).'&0[TAGS]='.$folder['ID']
            .'&0[RESPONSIBLE_ID]='.$this->getInvolvedEmployees()
            .'&0[AUDITORS]=3524'
            .'&0[DEADLINE]='.urlencode(date('c', (time() + (2 * 24 * 60 * 60))))
            .'&0[CREATED_BY]='.CFG['XMSTesting_user_id']
            .'&0[PARENT_ID]=61732'
            .'&0[UF_CRM_TASK][0]=C_18562'
            , '/task.item.add'
        );
    }

    public function attachFiles($taskId, $files)
    {
        $file_list = '';
        foreach ($files as $i => $file) {
            $file_list .= "&0[UF_TASK_WEBDAV_FILES][$i]=n{$file->ID}";
        }

        $this->curlPostRequest(
            '&TASKID='.$taskId
            .$file_list
            , '/task.item.update'
        );
    }

    protected function getInvolvedEmployees() : string
    {
        return "19";
    }

    protected function filterRecentlyCreatedFolder($lastFolders): array
    {
        $timeNow = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));

        return array_filter($lastFolders, function ($folder) use ($timeNow) {
            $timeFolderCreated = (new \DateTime($folder->CREATE_TIME))->add(new \DateInterval('PT1H')); //add 1 hour
            return $timeNow > $timeFolderCreated;
        });
    }

    protected function makeTaskTitle(array $folder): string
    {
        return "XMS Test {$folder['NAME']} is ready";
    }

    public function getFilesToAttach($filesInFolder, $filesInTask): array
    {
        return array_filter($filesInFolder, function($file) use ($filesInTask) {
            return (!in_array($file->NAME, $filesInTask));
        });
    }

    public function getOldFilesToRemoveFromDB($filesInFolder, $filesInTask): array
    {
        $filesInB24Folder = [];

        foreach ($filesInFolder as $file) {
            $filesInB24Folder[] = $file->NAME;
        }

        return array_diff($filesInTask, $filesInB24Folder);
    }

    /**
     * @param $taskId
     * @param $folder
     * @param $fileList
     */
    public function updateTaskDescription($taskId, $folder, $fileList)
    {
        $this->curlPostRequest(
            '&TASKID='.$taskId
            .'&0[DESCRIPTION]='.urlencode(
                (new XMS_TaskDescriptionMaker())->makeTaskDescription($folder, $fileList)
            ), '/task.item.update'
        );
    }
}
