<?php
declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

use PHPMailer\PHPMailer\PHPMailer;

class EmailSender
{
    protected static $excluded_from_mailing = [
        '1', // Maksim Ivanov
        '5' // Aleksei Butov
    ];

    public static function send(array $notifyees, string $task_link, string $title)
    {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'mx.xmtextiles.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'web_request@xmtextiles.com';
        $mail->Password = 'Qq}7ch5ENX';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->isHTML(true);
        $mail->FromName = 'Web Request';

        foreach ($notifyees as $notifyee) {
            if (in_array($notifyee['ID'], self::$excluded_from_mailing)) continue; // do not add to recipients
            $mail->addAddress($notifyee['EMAIL'], "{$notifyee['NAME']} {$notifyee['LAST_NAME']}");
        }
        $mail->Subject = $title;
        $mail->Body = 'New web-request task' . '<br />';
        $mail->Body .= "<a href=\"$task_link\">$title</a>";

        if (!$mail->send()) {
            echo 'Mailer Error: '.$mail->ErrorInfo;
        }
    }
}
