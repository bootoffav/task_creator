<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator\Traits;

trait B24DiskTalker
{

    public function onlyFiles(string $folder_id) : array
    {
        return array_values(array_filter($this->getChildren($folder_id), function ($child) {
            return $child->TYPE === 'file';
        }));
    }

    public function onlyFolders(string $folder_id) : array
    {
        return array_values(array_filter($this->getChildren($folder_id), function ($child) {
            return $child->TYPE === 'folder';
        }));
    }

    protected function getLastFolders(array $folders) : array
    {
        return array_values(array_filter($folders,
            function ($folder) {
                return time() - strtotime($folder->CREATE_TIME) < $this->getTimeframe();
            }));
    }

    private function getTimeframe()
    {
        return isset($this->checking_period_days)
            ? $this->checking_period_days * 24 * 60 * 60
                                     : 100 * 24 * 60 * 60;
    }

    protected function getChildren(string $folder_id) : array
    {
        return $this->curlRequest(CFG['webhook']
           .'disk.folder.getchildren?'
           .'id='.$folder_id);
    }
}