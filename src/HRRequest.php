<?php

namespace bootoffav\XMT\b24_task_creator;

class HRRequest extends TaskManager
{
    protected $number_filepath = '../hr_number';

    protected function makeTaskTitle(array $email) : string
    {
        return 'HR_request #'
                .str_pad($this->getNumber(), 3, '0', STR_PAD_LEFT)
                .' from '
                .$email['from_address']
                .' ('.date('d F Y', time()).')';
    }

    public function createTask(array $email) : \stdClass
    {
        return $this->curlRequest(
            CFG['hostname']
            .'/rest/task.item.add?auth='.$this->auth_key
            .'&&0[TITLE]='.urlencode($this->makeTaskTitle($email))
            .'&0[DESCRIPTION]='.urlencode($email['message_body'])
            .'&0[RESPONSIBLE_ID]='.$this->getInvolvedEmployees()
            .'&0[DEADLINE]='.urlencode(date('c', (time() + (2 * 24 * 60 * 60))))
            .'&0[CREATED_BY]='.CFG['task_creator_user_id']
            );
    }

    protected function getInvolvedEmployees() : string
    {
        return CFG['env'] === 'development'
               ? '1&0[ACCOMPLICES][0]=1&0[AUDITORS][0]=1'
               : '19&0[ACCOMPLICES][0]=1&0[AUDITORS][0]=19';
    }
}
