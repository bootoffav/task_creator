<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

class XMS_TaskDescriptionMaker
{
    protected $itemCheckingParams = [
        '1' => 'Full box weight (with Label on the box)',
        '2' => 'Rolls in the Open box',
        '3' => 'Sticker inside the roll',
        '4' => 'Width of the roll',
        '5' => 'Weight of the roll',
        '6' => 'Width of the tape (ruler)',
        '7' => 'Label on the box',
        '8' => 'Thickness of the box',
        '9' => 'Tape on the roller',
        '10' => 'Table of the Master',
        '11' => 'Video for Vertical burn (FR)',
        '12' => 'Tape on the fabric after wash (with Flash)',
        '13' => 'Tape on the fabric after wash (No Flash)',
        '14' => 'Wash report from the factory',
        '15' => 'Table of YuGe',
        '16' => 'R before wash in office',
        '17' => 'R (device in office)-tape after 25/50 wash by factory'
    ];

    public function makeTaskDescription(array $folder, array $fileList): string
    {
        $passed_params = $this->parseItemPassedParams($fileList, $folder['NAME']);
        $missed_params = $this->getItemMissedParams($passed_params);
        $fileList = $this->sortFileList($fileList, array_keys($passed_params));

        $li = '';
        foreach ($fileList as $file) {
            $li .= "[*][url={$file->DETAIL_URL}]{$file->NAME} "
            . $this->formParametersInFilelist($this->paramsOfFilename($file->NAME))
            . "[/url]";
        }

        $reportOfMissingParams = '';
        foreach ($missed_params as $item => $params) {
            $reportOfMissingParams .= PHP_EOL . PHP_EOL . $this->formReportOfMissingParams($item, $params);
        }

        return "[b]Folder:[/b] [url={$folder['DETAIL_URL']}]{$folder['NAME']}[/url]" . PHP_EOL . PHP_EOL .
            "[b]File list:[/b]" . PHP_EOL . '[list=1]' . $li . '[/list]'
            . $reportOfMissingParams;
    }

    protected function formParametersInFilelist($params) : string
    {
        if (empty($params)) {
            return '';
        }

        $ret_str = '';
        foreach ($params as $param) {
            $ret_str .= $this->itemCheckingParams[$param] . ', ';
        }

        $ret_str = substr($ret_str, 0, strlen($ret_str) - 2);
        return '(' . $ret_str . ')';
    }

    protected function parseItemPassedParams($fileList, string $order_name) : array
    {
        $passed_params = []; //items and their passed parameters
        try {
            foreach ($fileList as $file) {
                if (substr($file->NAME, -4) == '.jpg' or substr($file->NAME, -4) == '.mp4') {
                    $paramsOfFilename = $this->paramsOfFilename($file->NAME);
                    if ($paramsOfFilename) {

                        $item = substr($file->NAME, 0, strpos($file->NAME, ' '));
                        if ($item === $order_name) {
                            continue;
                        } // exclude same file name as order name

                        (isset($passed_params[$item])) ? : $passed_params[$item] = [];
                        $passed_params[$item] = array_merge(
                            $passed_params[$item], array_map('intval', $paramsOfFilename)
                        );
                    }
                }
            }

            $passed_params = array_map(function($item_params) {
                return array_values(array_unique($item_params));
            }, $passed_params);

          } catch (\Throwable $t) {
              echo "Order name: $order_name, " . $t->getMessage() . PHP_EOL;
          }
        return $passed_params;
    }

    protected function paramsOfFilename(string $filename)
    {
        $occurenceOf_1 = strpos($filename, '(');
        $occurenceOf_2 = strpos($filename, ')', $occurenceOf_1+1);
        
        if ($occurenceOf_1 === false or $occurenceOf_2 === false) {
            return [];
        }

        $params_of_current_filename = substr(
            $filename,
            $occurenceOf_1 + 1,
            $occurenceOf_2 - ($occurenceOf_1+1)
        );

        return explode(',', $params_of_current_filename);
    }

    protected function getItemMissedParams(array $passed_params) : array
    {
        $missed_params = [];
        foreach ($passed_params as $item => $params) {
            $diffs = array_values(array_diff(array_keys($this->itemCheckingParams), $params));
            $missed_params[$item] = $diffs;
        }

        return $missed_params;
    }

    protected function formReportOfMissingParams($item, $params) : string
    {
        $filling = "[COLOR=#FF0000]ITEM $item MISSES PICTURES OF:[/COLOR]" . PHP_EOL;
        $filling .= "[LIST]";
        
        foreach ($params as $param) {
            $filling .= "[*][COLOR=#FF0000]$param {$this->itemCheckingParams[$param]}[/COLOR]";
        }

        $filling .= "[/LIST]";
        return $filling;
    }

    protected function sortFileList($fileList, $items)
    {
        usort($fileList, function($a, $b) {
            return strnatcmp($a->NAME, $b->NAME);
        });

        return $fileList;
    }
}
