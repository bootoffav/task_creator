<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

use bootoffav\XMT\b24_task_creator\Traits\B24DiskTalker;

class Passport extends TaskManager
{
    use B24DiskTalker;
    // array of folder to run checking for tasks
    protected $folders;

    public function __construct()
    {
        $this->folders = self::getLastFolders($this->onlyFolders(CFG['passport']['base_folder']));
    }

    public function getFoldersContainPassports()
    {
        $foldersWithPassports = [];
        foreach ($this->folders as $folder) {
            $files = $this->onlyFiles($folder->ID);
            foreach ($files as $file) {
                if ($this->isPassportFile($file->NAME)) {
                    $foldersWithPassports[] = $this->getFolderStructure($folder, $files, $file);
                    break; // one passport file is enough per folder, files expected to be sorted asc by its ID
                }
            }
        }

        return $foldersWithPassports;
    }

    public function createTask(array $order) : \stdClass
    {
        return $this->curlRequest(
            CFG['webhook']
            .'task.item.add?'
            .'&0[TITLE]='.urlencode($this->makeTaskTitle($order))
            .'&0[DESCRIPTION]='.urlencode($this->makeTaskDescription($order))
            .'&0[TAGS]='.$order['passport_file']['id']
            .'&0[RESPONSIBLE_ID]='.$this->getInvolvedEmployees()
            .'&0[DEADLINE]='.urlencode(date('c', (time() + (2 * 24 * 60 * 60))))
            .'&0[CREATED_BY]='.CFG['tech_passport_user_id']
            .'&0[PARENT_ID]=114744'
            .'&0[UF_CRM_TASK][0]=C_16008'
            );
    }

    protected function getFolderStructure(
        \stdClass &$folder,
        array &$files,
        \stdClass &$passport_file
    ) : array {
        $structure['folder'] = [
            'name' => $folder->NAME,
            'url' => $folder->DETAIL_URL,
        ];
        foreach ($files as $file) {
            if ($file->ID === $passport_file->ID) {
                $structure['passport_file'] = [
                    'id' => $file->ID,
                    'name' => $file->NAME,
                    'url' => $file->DETAIL_URL,
                ];
            } else {
                $structure['other_files'][] = [
                    'name' => $file->NAME,
                    'url' => $file->DETAIL_URL,
                ];
            }
        }

        return $structure;
    }

    protected function getInvolvedEmployees() : string
    {
        return CFG['env'] === 'development'
        ? '1&0[ACCOMPLICES][0]=1&0[AUDITORS][0]=1'
        : '19&0[ACCOMPLICES][0]=33&0[ACCOMPLICES][1]=3696&0[AUDITORS][0]=19&0[AUDITORS][1]=97&0[AUDITORS][2]=7&0[AUDITORS][3]=3654';
    }

    protected function makeTaskTitle(array $order) : string
    {
        return 'Technical passport for '.$order['folder']['name'].' is ready';
    }

    protected function isPassportFile(string $name) : bool
    {
        return stripos($name, 'passport') !== false;
    }

    protected function makeTaskDescription(array $order) : string
    {
        $body = "[b]Order folder:[/b] [url={$order['folder']['url']}]{$order['folder']['name']}[/url]" . PHP_EOL
               ."[b]Passport:[/b] [url={$order['passport_file']['url']}]{$order['passport_file']['name']}[/url]" . PHP_EOL . PHP_EOL;
        if (!empty($order['other_files'])) {
            $body .= "[b]Other files:[/b]" . PHP_EOL . "[list=1]";
            foreach ($order['other_files'] as $file) {
                $body .= "[*][url={$file['url']}]{$file['name']}[/url]" . PHP_EOL;
            }
            $body .= '[/list]';
        }

        return $body;
    }

    /**
     * overloading cause disk.folder.getchildren doesn't provide next property in response.
     */
    protected function curlRequest(string $url, bool $check = false)
    {
        $result = [];
        $next = 0;
        $ch = curl_init();
        if ($check) {
            echo $url;
        }
        do {
            curl_setopt($ch, CURLOPT_URL, $url.'&start='.$next);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = json_decode(curl_exec($ch));
            if (isset($response->error)) {
                throw new \Exception($response->error_description, 1);
            }
            if (is_int($response->result)) {
                return $response;
            }
            $result = array_merge($result, $response->result);
            $next += 50;
            usleep(1000000); // 1 sec delay
        } while (!empty($response->result));
        curl_close($ch);

        return $result;
    }
}
