<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

class WebRequest extends TaskManager
{
    protected $number_filepath = '../request_number';
    protected $everytime_auditors = ['19', '9' , '1', '206', '27'];
    public $everytime_notifyees = [
        '19', // Vitaly Aliev
        '206', // Aleksandr Zhbannikov
        '1', // Maksim Ivanov
        '9', // Peter Grigoriev
    ]; 
    protected $responsible;

    public function createTask(array $email) : \stdClass
    {
        $taskTitle = $email['task_title'] ?? $this->makeTaskTitle($email);
        return $this->curlPostRequest(
           '&0[TITLE]=' . urlencode($taskTitle)
            . '&0[DESCRIPTION]=' . urlencode($email['message_body'])
            . '&0[RESPONSIBLE_ID]=' . $this->getResponsibleEmployees($email['country'], $taskTitle)
            . $this->getAuditorsPart($email['country'])
            . '&0[DEADLINE]=' . urlencode(date('c', (time() + (2 * 24 * 60 * 60))))
            . '&0[CREATED_BY]=' . $this->getCreator($email['from_address'])
            . $this->getUF()
            , '/task.item.add');
    }

    protected function getCreator($emailAddr) {
        return ($emailAddr === 'orders@b2bshop.xmtextiles.com') ? CFG['b2bshop_user_id'] : CFG['web_request_user_id'];
    }

    public function getEmployeeById($id): array {
         try {
            $result = $this->curlRequest(
                CFG['webhook']
                . 'user.get?'
                . "&&ID=$id"
            );
            return [
                'NAME' => $result[0]->NAME,
                'LAST_NAME' => $result[0]->LAST_NAME,
                'EMAIL' => $result[0]->EMAIL
            ];
         } catch (e) {
             return [];
         }
    }

    public function CreateCommentToNotifyAuditors($taskId, $country) {
        $auditors = $this->getAuditors($country);
        $auditorsAppeal = '';
        foreach ($auditors as $id) {
            $employee = $this->getEmployeeById($id);
            $auditorsAppeal .= "[USER=$id]{$employee['NAME']} {$employee['LAST_NAME']}[/USER] ";
        }
        return $this->curlPostRequest(
            "&0=$taskId"
            . "&fields[POST_MESSAGE]=$auditorsAppeal task created"
        , 'task.commentitem.add');
    }

    public function CreateComment($taskId, $message) {
        $employee = $this->getEmployeeById($this->responsible);
        return $this->curlPostRequest(
            "&0=$taskId"
            . "&fields[POST_MESSAGE]=[USER=$this->responsible]{$employee['NAME']} {$employee['LAST_NAME']}[/USER] $message"
        , 'task.commentitem.add');
    }

    protected function getFullNameForLead($body): array {
        if (strpos($body, 'Name:') !== false) {
            $start = mb_strpos($body, 'Name:') + strlen('Name:') + 1;
            $end = mb_strpos($body, "\n", $start);
            $fullName = mb_substr($body, $start, $end - $start);
        } else {
            $from = strpos($body, 'From: ') + strlen('From: ');
            $leftAngleBracket = strpos($body, '<');
            $fullName = trim(substr($body, $from, $leftAngleBracket - $from));
        }
        $fullName = explode(' ', $fullName);
        $fullName[0] = $fullName[0] ?? '';
        $fullName[1] = $fullName[1] ?? '';

        return $fullName;
    }

    public function createLead($email) {
        $fullName = $this->getFullNameForLead($email['message_body']);

        return $this->curlPostRequest(
            '&fields[TITLE]=Lead from Web-request #' . str_pad($this->getNumber(), 3, '0', STR_PAD_LEFT)
            . '&fields[NAME]=' . $fullName[0]
            . '&fields[LAST_NAME]=' . $fullName[1]
            . '&fields[SOURCE_ID]=40' // Source: Web-request
            . '&fields[STATUS_ID]=NEW'
            . '&fields[ASSIGNED_BY_ID]=' . $this->responsible ?? '189'
            . '&fields[CREATED_BY_ID]=' . CFG['web_request_user_id']
            . '&&fields[EMAIL][0][VALUE]=' . urlencode($email['from_address']) ?? ''
            . '&fields[EMAIL][0][VALUE_TYPE]=WORK'
            . '&params[REGISTER_SONET_EVENT]=Y'
            , '/crm.lead.add');
    }

    public function attachLeadToTask(string $leadId, int $taskId) {
        $leadToAdd = (object) [
            'TYPE' => 'L_',
            'ID' => $leadId
        ];
        array_push($this->entities, $leadToAdd);

        return $this->curlPostRequest(
            "&0=$taskId"
            . $this->getUF(1)
            , '/task.item.update');
    }

    public function getResponsibleForCountry(string $country) : array
    {
        if (!isset($country)) {
            return ['189'];
        }

        $responsible_for_country = [
            23 => ['Brazil'], // Natalia Demidova
            25 => ['United States Of America', 'Canada'], // Alina Clewlow (Dubs)
            161 => ['Czech Republic', 'Slovakia', 'Finland'], // Marek Krupala
            165 => ['Poland'], // Agnesa Lyskoit
            173 => ['Bulgaria'], // Veaceslav Cojocari
            181 => ['Spain'], // Beatriz Requeiro
            238 => ['Lithuania', 'Latvia', 'Estonia', 'Sweden', 'Norway', 'Ukraine', 'Bulgaria'], // Lilija Sinkevic
            310 => ['Romania', 'Slovenia', 'Croatia'], // Victor Copaci
            1606 => ['India', 'Pakistan', 'Bangladesh'], // Sanjay Thakur
            3630 => ['Germany', 'Austria', 'Switzerland', 'Denmark', 'Netherlands'], // Lucian Ciovica
            3736 => ['Greece'], // Fabian Dinu
            3898 => ['Kazakhstan'], // Aidar Auezov
            3901 => ['United Kingdom'], // Robert Petyko
            4416 => ['Portugal'], // Iago Aymerich
            5216 => ['Italy'], // Laura Menapace
            5240 => ['Turkey'], // Mert Derya
            5288 => ['France', 'Morocco', 'Tunisia', 'Algeria'], // Achraf EL Kamili
            5338 => ['Hungary'], // Lilla Sebo
        ];

        foreach ($responsible_for_country as $person_id => $person_countries) {
            if (in_array($country, $person_countries, true)) {
                $result[] = $person_id;
            }
        }

        return $result ?? ['189'];
    }

    private function getAuditorsForCountry(string $country) : array
    {
        if (!isset($country)) {
            return [];
        }

        $auditors_for_country = [
            173 => [
                'Spain', 'Portugal', 'Netherlands',
                'France', 'Belgium', 'Germany',
                'Austria', 'Switzerland', 'Denmark',
                'Turkey'
            ], // Veaceslav Cojocari
            310 => ['Romania', 'Germany', 'Austria', 'Switzerland', 'Denmark', 'Hungary'], // Victor Copaci
            5200 => ['Bangladesh'], // MD Abdullah AL Arman
            161 => ['United Kingdom'], // Marek Krupala
            2802 => ['Hungary'] // Radu Popescu
        ];

        foreach ($auditors_for_country as $person_id => $person_countries) {
            if (in_array($country, $person_countries, true)) {
                $result[] = $person_id;
            }
        }

        return $result ?? [];
    }
    
    public function makeTaskTitle(array $email) : string
    {
        switch ($email['type']) {
            case 'html':
                return 'Web_request #'
                    .str_pad($this->getNumber(), 3, '0', STR_PAD_LEFT)
                    .' from '
                    .$email['company_name']
                    . ($email['country'] ? ' (' . $email['country'] . ') ': '') 
                    .$email['from_address']
                    .' ('.date('d F Y', time()).')';
            case 'plain':
                return 'Web_request #'
                    .str_pad($this->getNumber(), 3, '0', STR_PAD_LEFT)
                    .' from '.$email['from_address']
                    .' ('.date('d F Y', time()).')';
        }
    }

    protected function getResponsibleEmployees(string $country, string $task_title) : string
    {
        if (CFG['env'] === 'development') {
            return '189&0[AUDITORS][0]=5';
        }
        if (substr($task_title, 0, 9) == "Web_order") {
            return '25';
        }
        $responsible = '';
        foreach ($this->getResponsibleForCountry($country) as $key => $person_id) {
            switch ($key) {
                case 0:
                    $responsible .= $person_id;
                    $this->responsible = $person_id;
                    break;
                default:
                    $responsible .= '&0[ACCOMPLICES]['.($key - 1)."]=$person_id";
            }
        }

        return $responsible;
    }

    protected function getAuditorsPart(string $country) : string {
        $i = 0;
        $auditorsPart = '';
        foreach ($this->getAuditors($country) as $id) {
            $auditorsPart .= "&0[AUDITORS][$i]=$id";
            $i++;
        }

        return $auditorsPart;
    }

    private function getAuditors(string $country) : array {
        $auditors = [];
        foreach ($this->getAuditorsForCountry($country) as $id) {
            array_push($auditors, $id);
        }
        foreach ($this->everytime_auditors as $id) {
            array_push($auditors, $id);
        }

        return $auditors;
    }
}


