<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

class XMS_DB
{
    public function __construct()
    {
        $this->conn = new \PDO('mysql:host=localhost;charset=utf8;dbname='.
            CFG['XMSTesting']['dbname'],
            CFG['XMSTesting']['dbuser'],
            CFG['XMSTesting']['dbpasswd']
        );
    }

    public function getFilesInTask($taskId) : array
    {
        $statement = $this->conn->prepare("SELECT fileName from map WHERE taskId='$taskId'");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_COLUMN, 0);
    }

    public function filesAddedToTask($taskId, $folderId, $files)
    {
        $statement = $this->conn->prepare("INSERT INTO map(taskId, folderId, fileId, excelFile, fileName) VALUES($taskId, $folderId, :fileId, :excelFile, :fileName)");
        $statement->bindParam('fileId', $fileId);
        $statement->bindParam('excelFile', $excelFile);
        foreach ($files as $file) {
            $statement->bindParam('fileName', $file->NAME);
            $excelFile = ($this->isExcelFile($file->NAME)) ? 1 : 0;
            $fileId = $file->ID;
            $statement->execute();
        }
    }

    protected function isExcelFile(string $name) : bool
    {
        if (strripos($name, 'test') !== false and strripos($name, '.xls') !== false) {
            return true;
        }
        return false;
    }

    public function removeOldFiles($oldFiles)
    {
        $statement = $this->conn->prepare("DELETE FROM map WHERE fileName=:fileName");
        foreach ($oldFiles as $file) {
            $statement->bindParam('fileName', $file);
            $statement->execute();
        }
    }
}