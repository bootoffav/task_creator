<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

/**
 * check web_request mailbox and get required email-addresses.
 */
class MailboxChecker
{
    protected $mailbox_handler;
    protected $mailsIds;
    protected $emails;

    public function __construct(\PhpImap\Mailbox $mailbox, $subject = null)
    {
        $this->mailbox_handler = $mailbox;
        if ($subject !== null) {
            $this->mailsIds = $this->mailbox_handler->searchMailbox('SUBJECT "'.$subject.'" SINCE "'.$this->getSincedate().'"');
            return;
        }
        $this->mailsIds = $this->mailbox_handler->searchMailbox('SINCE "'.$this->getSincedate().'"');
    }

    /**
     * refactor to only check switch statement once.
     */
    public function setEmailParameters(string $mail_login)
    {
        $parser_lookup = [
            'call_request@xmsilverline.es' => __NAMESPACE__.'\parsers\CallRequestEmailParser',
            'online_order@xmsilverline.es' => __NAMESPACE__.'\parsers\OrderEmailParser',
            'hr@xmtextiles.com' => __NAMESPACE__.'\parsers\HRRequestEmailParser',
            'web_request@xmtextiles.com' => __NAMESPACE__.'\parsers\WebRequestEmailParser',
            'samples_order@xmtextiles.com' => __NAMESPACE__.'\parsers\SampleRequestEmailParser',
        ];

        foreach ($this->mailsIds as $imapId) {
            try {
                $mail = $this->mailbox_handler->getMail($imapId);
                $mail->imapId = $imapId;
                $this->emails[] = $parser_lookup[$mail_login]::parse($mail);
            } catch (\Throwable $t) {
                echo $t->getMessage() , PHP_EOL;
            }
        }
    }

    public function getEmails() : array
    {
        return $this->emails ?? [];
    }

    private function getSincedate() : string
    {
        return date('d F Y', (time() - (10 * 24 * 60 * 60)));
    }

    public function moveTo(int $mailId, string $folder) : bool
    {
        return $this->mailbox_handler->moveMail($mailId, $folder);
    }
}
