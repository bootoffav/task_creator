<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

interface B24Communicator
{
    /**
     * Searching for relative CRM entities.
     *
     * @param $from_address email_address that is taken to find entity
     *
     * @return bool
     */
    public function findEntity(string $from_address) : bool;
    /**
     * Form part of get string for attaching relative crm entities.
     */
    public function createTask(array $email) : \stdClass;
}
