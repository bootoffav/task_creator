<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

class CallRequest extends TaskManager
{
    protected function getCountry(string $message_body)
    {
    }
    protected function getCompanyName(string $message_body)
    {
    }

    public function createTask(array $email) : \stdClass
    {
        return $this->curlRequest(
            CFG['hostname']
            .'/rest/task.item.add?auth='.$this->auth_key
            .'&&0[TITLE]='.urlencode($this->makeTaskTitle($email))
            .'&0[DESCRIPTION]='.urlencode($email['description'])
            .$this->getUF()
            .'&0[RESPONSIBLE_ID]='.$this->getInvolvedEmployees()
            .'&0[DEADLINE]='.urlencode(date('c', (time() + (2 * 24 * 60 * 60))))
            .'&0[CREATED_BY]='.CFG['task_creator_user_id']
            .'&0[PRIORITY]=2'
        );
    }

    protected function makeTaskTitle(array $email) : string
    {
        return 'Call_request to '
                .$email['telephone_number']
                .' from '
                .$email['from_name']
                .' ('
                .date('d F Y', time())
                .')';
    }

    protected function getInvolvedEmployees() : string
    {
        return CFG['env'] === 'development'
                  ? '1&0[ACCOMPLICES][0]=1&0[AUDITORS][0]=1'
                  : '169&0[ACCOMPLICES][0]=256&0[AUDITORS][0]=19&0[AUDITORS][1]=206';
    }
}
