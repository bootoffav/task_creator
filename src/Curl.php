<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

trait Curl
{
    protected $check = false;

    protected function curlRequest(string $url)
    {
        $response = [];
        $ch = curl_init();
        do {
            if ($this->check) {
                echo $url, PHP_EOL;
            }
            $next = $request->next ?? 0;
            curl_setopt($ch, CURLOPT_URL, $url.'&start='.$next);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $request = json_decode(curl_exec($ch));

            if (isset($request->error)) {
                throw new \Exception($request->error_description, 1);
            }

            if (empty($request->result)) {
                break;
            }

            if (is_array($request->result)) {
                $response = array_merge($response, $request->result);
                continue;
            }

            return $request;
        } while (isset($request->next));
        curl_close($ch);

        return $response;
    }

    protected function curlPostRequest(string $url, string $method)
    {
        if ($this->check) {
            echo $url, PHP_EOL;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, CFG['webhook'] . $method);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $request = json_decode(curl_exec($ch));
        curl_close($ch);

        if (isset($request->error)) {
            throw new \Exception($request->error_description, 1);
        }

        if (is_int($request->result)) {
            return $request;
        }

    }
}
