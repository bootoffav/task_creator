<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

class Order extends TaskManager
{
    public function createTask(array $email) : \stdClass
    {
        return $this->curlRequest(
            CFG['hostname']
            .'/rest/task.item.add?auth='.$this->auth_key
            .'&&0[TITLE]='.urlencode($this->makeTaskTitle($email))
            .'&0[RESPONSIBLE_ID]='.$this->getInvolvedEmployees()
            .'&0[DEADLINE]='.urlencode(date('c', (time() + (4 * 24 * 60 * 60))))
            .'&0[CREATED_BY]='.CFG['task_creator_user_id']
        );
    }

    protected function getInvolvedEmployees() : string
    {
        return CFG['env'] === 'development'
               ? '1&0[ACCOMPLICES][0]=1&0[AUDITORS][0]=1'
               : '314&0[ACCOMPLICES][0]=181&0[ACCOMPLICES][1]=470&0[ACCOMPLICES][2]=264&0[AUDITORS][0]=19&0[AUDITORS][1]=206';
    }

    public function attachFiles($task_id, $file)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, CFG['hostname'].'/rest/task.item.addfile');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, [
                'auth' => $this->auth_key,
                'TASK_ID' => $task_id,
                'FILE[NAME]' => $file,
                'FILE[CONTENT]' => base64_encode(file_get_contents($file)),
            ]);
        $out = curl_exec($curl);
        curl_close($curl);
    }

    protected function makeTaskTitle(array $email) : string
    {
        return 'XMSilverline.es_order #'
               .str_pad($email['order_number'], 3, '0', STR_PAD_LEFT)
               .' ('.date('d F Y', time()).')';
    }
}
