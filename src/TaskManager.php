<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

abstract class TaskManager //implements B24Communicator
{
    use Curl;

    public $entities = [];

    abstract protected function makeTaskTitle(array $email) : string;

    protected function getUF($paramPosition = 0) : string
    {
        $uf = '';
        for ($i = 0; $i < count($this->entities); ++$i) {
            $uf .= '&' . $paramPosition . "[UF_CRM_TASK][$i]=" . $this->entities[$i]->TYPE . $this->entities[$i]->ID;
        }

        return $uf;
    }
    
    /*
     ** function checks if a company, a contact or a lead exist by email. If they exist it returns all entities, otherwise false is returned.
     */
    public function findEntity(string $from_address) : bool
    {
        if ($from_address === '') {
            return false;
        }
        $this->entities = [];
        $response = [];
        $response = $this->curlRequest(
            CFG['webhook']
            .'/crm.company.list?'
            .'&&filter[EMAIL]='.$from_address
            .'&select[0]=ID'); //searching for company
            if (count($response) > 0) {
                $response[0]->TYPE = 'CO_';
                array_push($this->entities, $response[0]);
            }
        $response = $this->curlRequest(
            CFG['webhook']
            .'/crm.contact.list?'
            .'&&filter[EMAIL]='.$from_address
            .'&select[0]=ID'); //searching for contact
        if (count($response) > 0) {
            $response[0]->TYPE = 'C_';
            array_push($this->entities, $response[0]);
        }
        $response = $this->curlRequest(
            CFG['webhook']
            .'/crm.lead.list?'
            .'&&filter[EMAIL]='.$from_address
            .'&select[0]=ID'); //searching for lead
        if (count($response) > 0) {
            $response[0]->TYPE = 'L_';
            array_push($this->entities, $response[0]);
        }

        return (count($this->entities) > 0) ? true : false;
    }

    public function attachFiles($task_id, $attachments)
    {
        foreach ($attachments as $attachment) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, CFG['webhook'].'/task.item.addfile');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, [
                'TASK_ID' => $task_id,
                'FILE[NAME]' => $attachment->name,
                'FILE[CONTENT]' => base64_encode(file_get_contents($attachment->filePath)),
            ]);
            $out = curl_exec($curl);
            curl_close($curl);
            unlink($attachment->filePath);
        }
    }

    protected function getNumber(string $message_body = '') : string
    {
        $file = fopen($this->number_filepath, 'r');
        $number = trim(fgets($file));
        fclose($file);

        return $number;
    }

    public function increaseNumber()
    {
        $number = $this->getNumber();
        $file = fopen($this->number_filepath, 'w');
        fputs($file, ((string) ++$number));
    }

    /**
     * @param  string $tag
     * @return false or taskID 
     */
    public function taskExists(string $tag)
    {
        $i = 0;
        $not_exists = 0;
        while ($i++ < 3) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, CFG['webhook']
                    .'/task.item.list?'
                    .'&&0[ID]=desc&&1[TAG]='.$tag);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = json_decode(curl_exec($ch));
            if ($response->total > 0) {
                return $response->result[0]->ID;
            }
            if (empty($response->result)) {
                ++$not_exists;
            }
        }
        curl_close($ch);

        return $not_exists !== 3;
    }

    public function notifyResponsiblesToPersonalChat($notifyees, $task_link) : void {
        foreach ($notifyees as $notifyee) {
            $this->curlPostRequest(
                 'MESSAGE=' . urlencode("[B]New web-request task[/B]\n $task_link")
                . "&DIALOG_ID={$notifyee['ID']}"
                , 'im.message.add');
        }
    }
}
