<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator\parsers;

class HRRequestEmailParser implements EmailParser
{
    public static function parse(\PhpImap\IncomingMail $mail) : array
    {
        $message_body = self::clearBody($mail->textHtml);

        return [
            'imap_id'      => $mail->imapId,
            'message_body' => $message_body,
            'from_address' => self::getFromAddress($message_body),
            'attachments' => $mail->getAttachments(),
        ];
    }

    public static function clearBody(string $message_body) : string
    {
        $message_body = str_replace('<br>', "\n", $message_body);

        return trim(strip_tags($message_body));
    }

    public static function getFromAddress($message_body) : string
    {
        $start = mb_strpos($message_body, 'E-mail: ') + 7;
        $length = mb_strpos($message_body, 'Form ID: ') - $start;

        return trim(mb_substr($message_body, $start, $length));
    }
}
