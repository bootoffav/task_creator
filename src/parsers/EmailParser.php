<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator\parsers;

interface EmailParser
{
    public static function parse(\PhpImap\IncomingMail $mail) : array;
}
