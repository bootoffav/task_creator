<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator\parsers;

class WebRequestEmailParser implements EmailParser
{
    public static function parse(\PhpImap\IncomingMail $mail): array
    {
        if (substr($mail->subject, 0, 9) == "Web_order") {
            return [
                'imap_id' => $mail->imapId,
                'type' => 'plain',
                'message_body' => $mail->textPlain,
                'from_address' => key($mail->replyTo),
                'company_name' => self::getCompanyName($mail->textPlain),
                'url' => 'https://www.xmsilverline.com',
                'country' => self::getCountry($mail->textPlain),
                'task_title' => $mail->subject,
                'attachments' => []
            ];
        }

        $fromAddress = substr($mail->fromAddress, 0, 12);
        if (in_array($fromAddress, ["web_request@", "xina@xmtexti"])) {
            $type = 'html';
            $message_body = self::clearHTMLBody($mail->textPlain ?: $mail->textHtml, $mail->subject);
        } else {
            $type = 'plain';
            $message_body = self::clearPlainBody($mail->textPlain);
        }

        // cut message body if request is from mailing campaign
        $message_body = self::parseMessageBody($message_body);
        return [
            'imap_id' => $mail->imapId,
            'type' => $type,
            'message_body' => $mail->subject . "\n" . $message_body,
            'from_address' => self::getFromAddress($message_body, $type),
            'attachments' => $mail->getAttachments(),
            'company_name' => ($type === 'html') ? self::getCompanyName($message_body) : '',
            'url' => ($type === 'html') ? self::getURL($message_body) : '',
            'country' => ($type === 'html') ? self::getCountry($message_body) : ''
        ];
    }

    protected static function parseMessageBody(string $body): string
    {
        $isFromMailing = strpos($body, '<mailto:info@xmtextiles.com>');
        return ($isFromMailing !== false) ? substr($body, 0, $isFromMailing) : $body;
    }

    protected static function clearHTMLBody(string $body, string $subject): string
    {
        $body = preg_replace("/<br>|<br\/>/", "\r\n", $body);
        $body = explode("\r\n",$body); // array of strings
        
        // removes strings containing only whitespaces
        $body = array_filter($body, function($str) {
            return strlen(trim($str));
        });

        // trim all strings and glue back to string
        $body = implode("\n", array_map(function ($str) {
            return trim($str);
        }, $body));
        if (strpos($subject, '[Request]') !== false) {
            $body = substr($body, strpos($body, 'Name:'));
        }
        if (strpos($subject, 'New request from') === 0 || substr($subject, 0, 12) === 'Request from' || substr($subject, 0, 21) === 'Request a ticket from') {
            $body = self::makeMessageBodyForWPForm($body);
        }

        return $body;
    }

    protected static function makeMessageBodyForWPForm($body): string
    {
        $getValueOf = function (string $from, string $till = null) use ($body)
        {
            $start = mb_strpos($body, $from);
            try {
                $end = mb_strpos($body, $till);
            } catch (\Throwable $th) {
                return trim(mb_substr($body, $start));
            }
            if ($start === false || $end === false) {
                return '';
            }
            $startingOffset = mb_strlen($from);
            $endingOffset = mb_strlen($till);
            $start = $start + $startingOffset;
            $length = $end - $start;
            $value = trim(mb_substr($body, $start, $length));
            return $value;
        };
        
        $name = $getValueOf('--- Name ---', '--- Your email ---');
        $email = $getValueOf('--- Your email ---', '--- Your phone number ---');
        $telephone = $getValueOf('--- Your phone number ---', '--- Your company ---');
        $company = $getValueOf('--- Your company ---', '--- Choose your country ---');
        $country = $getValueOf('--- Choose your country ---', '--- Your message ---');
        $request = $getValueOf('--- Your message ---', 'Site URL:');
        $rest = $getValueOf('Site URL:', 'IP:');
        $IP = $getValueOf('IP:') ?? '';

        return <<<EOT
Name: $name
E-mail: $email
Telephone: $telephone
Company name: $company
Country: $country
Request: $request

$rest
$IP
EOT;
    }

    protected static function clearPlainBody(string $message_body): string
    {
        if (mb_strpos($message_body, 'From: ')) {
            return trim((mb_substr($message_body, mb_strpos($message_body, 'From: '))));
        }
        
        if (mb_strpos($message_body, 'Отправитель: ')) {
            return trim((mb_substr($message_body, mb_strpos($message_body, 'Отправитель: '))));
        }

        return $message_body;
    }

    protected static function getFromAddress(string $message_body, &$type): string
    {
        // Woocommerce template Quote request
        if (substr($message_body, 0, 13) === 'Quote request' or substr($message_body, 0, 12) === 'Request from' or substr($message_body, 0 , 24) === 'Solicitud de presupuesto') {
            // search for E-mail:
            $start = strpos($message_body, 'Email: ') + strlen('Email: ');
            $end = strpos($message_body, "\n", $start);
            return trim(substr(
                $message_body,
                $start,
                $end - $start 
            ));
        }
        if (substr($message_body, 0, 30) === 'http://b2bshop.xmtextiles.com/') {
            return 'orders@b2bshop.xmtextiles.com';
        }
        #XMSilverline.es
        if (substr($message_body, 0, 5) === 'Usted') { #Usted - first word in message body of form comes from XMS.es
            $trimmed = trim(substr($message_body, 0, strpos($message_body, 'Mensaje:')));
            return trim((substr($trimmed, strrpos($trimmed, ' '))));
        }
        #WooCommerce plain form
        if (substr($message_body, 0, 22) === '= New customer order =') {
            $email = trim(strrchr($message_body, "\r\n"));
            return $email;
        }
        switch ($type) {
            case 'html':
                //Normal form clearing
                if (mb_strpos($message_body, 'E-mail: ')) {
                    $start = mb_strpos($message_body, 'E-mail:') + 8;
                    $length = mb_strpos($message_body, 'Telephone:') - $start;
                    return trim(mb_substr($message_body, $start, $length));
                }
                //html format but not form submission
                if (mb_strpos($message_body, '&lt;')) {
                    $start = mb_strpos($message_body, '&lt;') + 4;
                    $length = mb_strpos($message_body, '&gt;') - $start;
                    return trim(mb_substr($message_body, $start, $length));
                }
            case 'plain':
                //From: "Craig R" <craig@orkatechnicalworkwear.com>
                $start = mb_strpos($message_body, '<');
                $length = mb_strpos($message_body, '>') - $start;

                if ($start !== false and $length !== false) {
                    $email = mb_substr($message_body, $start + 1, $length - 1);
                    return filter_var($email, FILTER_VALIDATE_EMAIL) ?: '';
                }

                //From: info@npwear.dk
                $start = mb_strpos($message_body, 'From: ');
                $length = mb_strpos($message_body, "\n");
                if ($start !== false and $length !== false) {
                    return filter_var(trim(mb_substr($message_body, $start + 6, $length)), FILTER_VALIDATE_EMAIL) ?: '';
                }
        }
    }

    protected static function getCompanyName(string $message_body): string
    {
        // XMS Web order handle
        $billing_address_needle = strpos($message_body, 'BILLING ADDRESS');
        if ($billing_address_needle !== false) {
            $body_with_offset = substr($message_body, $billing_address_needle);
            $strings = preg_split("/\r\n|\n|\r/", $body_with_offset);
            return $strings[3] ?? '';
        }
        unset($billing_address_needle);

        if (substr($message_body, 0, 22) === '= New customer order =') {
            preg_match_all('/\n(.*)\n/', $message_body, $matches);
            return trim(array_reverse($matches[0])[1]);
        }
        // YITH Woocommerce Quote Request
        if (substr($message_body, 0, 13) === 'Quote request') {
            $start = strpos($message_body, 'Company: ') + strlen('Company: ');
            $end = strpos($message_body, "\n", $start);
            return trim(substr(
                $message_body,
                $start,
                $end - $start 
            ));
        };
        $start = mb_strpos($message_body, 'Company name:') + 14;
        $length = mb_strpos(mb_substr($message_body, $start), "\n");

        return ucwords(trim(mb_substr($message_body, $start, $length)));
    }

    protected static function getCountry(string $message_body): string
    {
        // XMS Web order handle
        $customer_country = strpos($message_body, 'Customer country:');
        if ($customer_country !== false) {
            $end = strpos($message_body, "\n", $customer_country);
            $start = $customer_country + 18; // 18 is a length of Customer country:
            return trim(substr($message_body, $start, $end - $start));
        }
        unset($customer_country);

        // XMT Web order handle
        if (substr($message_body, 0, 22) === '= New customer order =') {
            preg_match_all('/\n(.*)\n/', $message_body, $matches);
            return trim(array_reverse($matches[0])[0]);
        }
        // YITH Woocommerce Quote Request
        if (substr($message_body, 0, 13) === 'Quote request') {
            $start = strpos($message_body, 'Country: ') + strlen('Country: ');
            $end = strpos($message_body, "\n", $start);
            return trim(substr(
                $message_body,
                $start,
                $end - $start 
            ));
        };
        $start = mb_strpos($message_body, 'Country:') + 9;
        $length = mb_strpos($message_body, 'Request:') - $start;
        return $start === 9 ? '' : ucwords(trim(mb_substr($message_body, $start, $length)));
    }

    protected static function getURL(string $message_body): string
    {
        $start = mb_strpos($message_body, 'Page:') + 6;
        $length = mb_strpos($message_body, 'IP:') - $start;

        return urlencode(trim(mb_substr($message_body, $start, $length)));
    }
}
