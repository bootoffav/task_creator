<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator\parsers;

class CallRequestEmailParser implements EmailParser
{
    public static function parse(\PhpImap\IncomingMail $mail) : array
    {
        return [
            'imap_id'   => $mail->imapId,
            'from_name' => $mail->fromName,
            'from_address' => $mail->headers->reply_toaddress,
            'description' => self::getDescription($mail->textPlain),
            'telephone_number' => self::getTelephoneNumber($mail->textPlain),
        ];
    }

    protected static function getDescription(&$message_body) : string
    {
        $start = mb_strpos($message_body, 'Time:') + 5;
        $length = mb_strpos($message_body, 'Tu teléfono:') - $start;

        return trim(mb_substr($message_body, $start, $length));
    }

    protected static function getTelephoneNumber(&$message_body) : string
    {
        $start = mb_strpos($message_body, 'Tu teléfono:') + 13;

        return trim(mb_substr($message_body, $start));
    }
}
