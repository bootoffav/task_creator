<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator\parsers;

class OrderEmailParser implements EmailParser
{
    public static function parse(\PhpImap\IncomingMail $mail) : array
    {
        return [
            'message_body' => $mail->textHtml,
            'order_number' => self::getOrderNumber($mail->subject),
            'imap_id' => $mail->imapId
        ];
    }

    public static function getOrderNumber(string $subject) : string
    {
        $start = mb_strpos($subject, '(') + 1;
        $length = mb_strpos($subject, ')') - $start;

        return trim(mb_substr($subject, $start, $length));
    }
}
