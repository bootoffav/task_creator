<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator\parsers;

class SampleRequestEmailParser implements EmailParser
{
    public static function parse(\PhpImap\IncomingMail $mail) : array
    {
        return [
            'message_body' => trim($mail->textPlain),
            'from_address' => self::getFromAddress($mail->textPlain),
            'imap_id' => $mail->imapId,
            'offices' => self::getOfficeName($mail->textPlain),
            'samples_number' => self::getNumber($mail->textPlain),
            'company_name' => self::getCompanyName($mail->textPlain),
            'country' => self::getCountry($mail->textPlain),
            'byManager' => self::byManager($mail->textPlain),
        ];
    }

    protected static function getFromAddress(string $message_body) : string
    {
        $start = mb_strpos($message_body, '5. E-mail:') + 11;
        $length = mb_strpos($message_body, '6. Telephone:') - $start;

        return trim(mb_substr($message_body, $start, $length));
    }

    protected static function getOfficeName(string $message_body) : array
    {
        $start = mb_strpos($message_body, 'Office:') + 8;
        $length = mb_strpos($message_body, '1. Company:') - $start;

        return explode(', ', trim(mb_substr($message_body, $start, $length)));
    }

    protected static function getNumber(string $message_body) : string
    {
        if (strpos($message_body, '№ ')) {
            return trim((substr($message_body, strpos($message_body, '№ ') + 4, 3)));
        }

        return '';
    }

    protected static function getCompanyName(string $message_body) : string
    {
        if (mb_strpos($message_body, '1. Company:') && mb_strpos($message_body, '2. Country:')) {
            $start = mb_strpos($message_body, '1. Company:') + 12;
            $length = mb_strpos($message_body, '2. Country:') - $start;

            return ucwords(trim(mb_substr($message_body, $start, $length)));
        }

        return '';
    }

    protected static function getCountry(string $message_body) : string
    {
        $start = mb_strpos($message_body, '2. Country:') + 12;
        $length = mb_strpos($message_body, '3. Address:') - $start;

        return ucwords(trim(mb_substr($message_body, $start, $length)));
    }

    protected static function byManager(string $message_body) : string
    {
        $sub_mb = mb_substr($message_body, 0, mb_strpos($message_body, '@'));
        $start = mb_strpos($message_body, 'samples:') + 9;
        $length = mb_strrpos($sub_mb, ' ') - $start;

        return trim(mb_substr($sub_mb, $start, $length));
    }
}
