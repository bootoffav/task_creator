<?php

declare(strict_types=1);

namespace bootoffav\XMT\b24_task_creator;

class SampleRequest extends TaskManager
{
    public function createTask(array $email) : \stdClass
    {
        $this->findEntity($email['from_address']);

        $title = $this->makeTaskTitle($email);
        $response = $this->curlRequest(
            CFG['hostname']
            .'/rest/task.item.add?auth='.$this->auth_key
            .'&&0[TITLE]='.urlencode($title)
            .'&0[DESCRIPTION]='.urlencode($email['message_body'])
            .$this->getUF()
            .'&0[RESPONSIBLE_ID]='.$this->getInvolvedEmployees($email)
            .'&0[DEADLINE]='.urlencode(date('c', (time() + (2 * 24 * 60 * 60))))
            .'&0[CREATED_BY]='.CFG['samples_order_user_id']
            );

        return $response;
    }

    private function getInvolvedEmployees(array $email) : string
    {
        if (CFG['env'] === 'development') {
            return '1&0[ACCOMPLICES][0]=1&0[AUDITORS][0]=1';
        }

        $responsible_in_office = [
            'Madrid' => ['169', '256'],
            'Vilnus' => ['159'],
            'Shanghai' => ['35', '45', '33'],
        ];
        $responsible = $this->getEmployeeID($email['byManager']);
        $i = 0;
        foreach ($email['offices'] as $office) {
            foreach ($responsible_in_office[$office] as $person_id) {
                $responsible .= '&0[ACCOMPLICES]['.$i++."]=$person_id";
            }
        }

        return $responsible.'&0[AUDITORS][0]=19&0[AUDITORS][1]=169&0[AUDITORS][2]=256&0[AUDITORS][3]=173';
    }

    protected function makeTaskTitle(array $email) : string
    {
        return 'Sample_request #'
                .str_pad($email['samples_number'], 3, '0', STR_PAD_LEFT)
                .' from '.$email['company_name']
                .' ('.$email['country'].') '
                .$email['from_address']
                .' - by '.$email['byManager'];
    }

    protected function getEmployeeID($manager) : string
    {
        $length = (strpos($manager, ' ')) ?: false;
        if ($length === false) {
            return '19'; // Vitaly Aliev in case of error
        }
        $name = substr($manager, 0, $length);
        $last_name = substr($manager, $length + 1);
        $response = $this->curlRequest(
            CFG['hostname']
            .'/rest/user.get?auth='.$this->auth_key
            .'&NAME='.$name
            .'&LAST_NAME='.$last_name);
        if (empty($response)) {
            return '19'; //Vitaly Aliev in case of nonfinding
        }

        return (string) $response[0]->ID;
    }
}
